## Hangman
Hangman ist ein einfaches Programm, welches das beliebte Hangman-Spiel umsetzt.

## Hintergrund
Das Programm war eine Aufgabenstellung im dritten Jahrgang. Geschrieben ist es in der Programmiersprache Java und setzt das MVC-Prinzip praktisch um.

## Start
Zum Starten der Anwendung muss nur die HangmanController-Klasse gestartet werden. 

## Funktionen
Es wird nach Begriffen gesucht, die in der Datei *liste.txt* gespeichert sind. In grafischen Oberfläche selber kann man aber Textdateien mit eigenen Wörten einlesen.
