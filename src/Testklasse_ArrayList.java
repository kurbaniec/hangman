import urbaniec.model.Wortliste;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Die Klasse testet die Klasse Wortliste, besonders die Methoden toString
 * und filter.
 * @author Kacper Urbaniec
 * @version 2017-11-13
 */
public class Testklasse_ArrayList {
    public static void main(String[] args) {
        // Array Liste für Wortliste erstellen
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Test", "ABC", "Array", "Liste", "T", "Super"));
        // Wortliste-Objekt erstellen
        Wortliste wl = new Wortliste("-", list);
        // toString testen
        System.out.println("Wortliste: " + wl.toString());
        // filter mit Länge 4 testen und ausgeben
        wl.filter(4);
        System.out.println("Alle Wörter mit weniger als 4 Zeichen werden entfernt");
        System.out.println("Neue Wortliste: " + wl.toString());
    }
}
