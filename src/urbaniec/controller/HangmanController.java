package urbaniec.controller;

import urbaniec.model.Hangman;
import urbaniec.model.Wortliste;
import urbaniec.view.HangmanFrame;
import urbaniec.view.HangmanGraphics;
import urbaniec.view.HangmanPanel;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

public class HangmanController implements ActionListener {
    private Hangman logik;
    private HangmanPanel panel;
    private HangmanFrame frame;
    private String file;
    private ArrayList<String> liste;

    public HangmanController() throws java.io.IOException {
        file = "liste.txt";
        logik = new Hangman(file);
        panel = new HangmanPanel(this);
        frame = new HangmanFrame("HANGMAN - The Game", panel);
        panel.setTitle(logik.getZustand());
        panel.revalidate();
        panel.repaint();
    }

    public void reset() throws java.io.IOException {
        liste = logik.getWortliste();
        logik = new Hangman(file, liste);
        frame.remove(panel);
        panel = new HangmanPanel(this);
        frame.add(panel);
        panel.setTitle(logik.getZustand());
        panel.revalidate();
        panel.repaint();
    }

    /**
     * Regelt die Ereignissteuerung.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        if (s.equals("Unten0") || s.equals("Unten1") || s.equals("Unten2") || s.equals("Unten3") || s.equals("Unten4")) {
            switch (s) {
                case "Unten0":
                    try {
                        reset();
                    }
                    catch (java.io.IOException ioe) {
                        System.out.println("Kritischer Fehler");
                    }
                    break;
                case "Unten1":
                    String eingabe = JOptionPane.showInputDialog(null, "Wort eingeben:");
                    try {
                        logik.setTxt(eingabe);
                    }
                    catch (java.io.IOException ioe) {
                        System.out.println("Kritischer Fehler");
                    }
                    break;
                case "Unten2":
                    panel.disableAllButtons();
                    JOptionPane.showMessageDialog(null, "Lösungswort: " + logik.getWort());
                    try {
                        reset();
                    }
                    catch (java.io.IOException ioe) {
                        System.out.println("Kritischer Fehler");
                    }
                    break;
                case "Unten3":
                    JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                    int returnValue = jfc.showOpenDialog(null);
                    if (returnValue == JFileChooser.APPROVE_OPTION) {
                        File file = jfc.getSelectedFile();
                        this.file = jfc.getName(file);
                    }
                    logik.setPath(this.file);
                    break;
                case "Unten4":
                    String fileName = JOptionPane.showInputDialog(null, "Geben Sie den Dateinamen ein:");
                    try {
                        PrintWriter out = new PrintWriter(fileName+".txt");
                        /**String[] wl = logik.getWortliste();
                        for(int i = 0; i < wl.length; i++) {
                            out.println(wl[i]);
                        }*/
                        for(String elem : logik.getWortliste()) {
                            out.println(elem);
                        }
                        out.close();
                    }
                    catch(java.io.FileNotFoundException jifn) {
                        System.out.println("Kritischer Fehler");

                    }

            }
        }
        else {
            char l = s.charAt(0);
            logik.test(l);
            String f = new String(logik.getFalsch());
            if (f.contains("" + l)) {
                int c = panel.getCounter();
                c++;
                panel.setCounter(c);
                panel.setFehler(logik.getFalsch());
            }
            panel.disableButton(l);
            panel.setTitle(logik.getZustand());
            panel.revalidate();
            panel.repaint();
            if (new String(logik.getZustand()).equals(logik.getWort())) {
                panel.disableAllButtons();
                JOptionPane.showMessageDialog(null, "Du hast gewonnen!");
                try {
                    reset();
                } catch (java.io.IOException ioe) {
                    System.out.println("Kritischer Fehler");
                }
            } else {
                if (logik.getZug() == 10) {
                    panel.disableAllButtons();
                    JOptionPane.showMessageDialog(null, "Verloren");
                    JOptionPane.showMessageDialog(null, logik.getWort() + " war gesucht!");
                    try {
                        reset();
                    } catch (java.io.IOException ioe) {
                        System.out.println("Kritischer Fehler");
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            new HangmanController();
        } catch (java.io.IOException ioe) {
            System.out.println("Textdatei(liste.txt) für die Wortliste fehlt!");
        }
    }


}
