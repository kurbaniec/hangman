package urbaniec.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Consumer;

/**
 * Die Wortliste Klasse ist zuständig für die Liste der Wörter für das Hangman-Spiel,
 * sowie für die Auswahl eines Wortes.
 *
 * @author Kacper Urbaniec
 * @version 2017-09-06
 */
public class Wortliste {
    private String path;
    private ArrayList<String> wortliste;
    private String zWort;


    /**
     * Erstellt ein neues Wortliste Objekt aus einer Textdatei.
     * @param file - Dateiname der Wortliste
     * @throws IOException
     */
    public Wortliste(String file) throws IOException {
        path = file;
        getTxt();
        zufall();
    }

    /**
     * Erstellt ein neues Wortliste Objekt.
     * @param file - Dateiname der Wortliste
     * @param wortliste - Derzeitige, zu weiterführende Liste
     */
    public Wortliste(String file, ArrayList<String> wortliste) {
        path = file;
        this.wortliste = wortliste;
        zufall();
    }

    /**
     * Setzt den neuen Namen der Wortliste-Datei.
     * @param path - Der neue Dateiname
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * WIRD NICHT VERWENDET
     * Setzt die neu Wortliste.
     * @param wortliste - Die neue Wortliste
     */
    public void setWortliste(ArrayList<String> wortliste) {
        this.wortliste = new ArrayList<>(wortliste.size());
        for(String elem : wortliste) {
            this.wortliste.add(elem);
        }
    }

    /**
     * Gibt das zufällig ausgewählte Wort zurück
     * @return Das zufällige Wort als String
     */
    public String getzWort() {
        return this.zWort;
    }

    /**
     * Gibt die derzeitige Wortliste zurück.
     * @return - Die derzeitige Wortliste
     */
    public ArrayList<String> getWortliste() {
        return wortliste;
    }

    /**
     * Wählt ein Wort aus der Wortliste zufällig aus.
     */
    public void zufall() {
        Random r = new Random();
        this.zWort = wortliste.get(r.nextInt(wortliste.size()));
    }

    /**
     * Die Methode holt Wörter für das Hangmanspiel aus einer externen Datei.
     * @throws IOException
     */
    public void getTxt() throws IOException {
        // txt Datei einlesen und String-Array mit
        // Wörtern füllen
        BufferedReader inputStream = null;
        try {
            this.wortliste = new ArrayList<>();
            // this.wortliste[0] = new String();
            inputStream = new BufferedReader(new FileReader(path));
            boolean nx = true;
            String test;
            for (int i = 0; nx; i++) {
                // Schaut ob man weiter die Datei überprüfen soll
                test = inputStream.readLine();
                if (test != null) {
                    this.wortliste.add(i, test);
                } else {
                    nx = false;
                }
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * WIRD NICHT VERWENDET
     * Speichert ein neues Wort in die externe Datei.
     * @param wort - Das neue Wort für die externe Datei
     * @throws IOException
     */
    public void setTxtLegacy(String wort) throws IOException {
        PrintWriter outputStream = null;
        outputStream = new PrintWriter(new FileWriter(path));
        for (int i = 0; i < this.wortliste.size(); i++) {
            //while(this.wortliste[i] != null) {
            outputStream.println(this.wortliste.get(i));
            //}
        }
        outputStream.println(wort);
        if (outputStream != null) {
            outputStream.close();
        }
    }


    /**
     * Speichert ein neues Wort zu Wortliste, aber nicht zur
     * externen Datei.
     * @param wort - Das neue Wort für die Wortliste
     */
    public void setTxt(String wort) {
        this.wortliste.add(wort);
    }

    /**
     * Gibt die Wortliste formatiert als String aus.
     * @return die Wortliste als String
     */
    @Override
    public String toString() {
        String s = "";
        for(String elem : this.wortliste) {
            s += elem + ", ";
        }
        s = s.substring(0, s.length()-2);
        return s;
    }

    /**
     * Löscht alle Wörter in der Wortliste, die kürzer als die
     * übergebene Länge sind.
     * @param laenge die Länge, die alle Wörter der Wortliste erfüllen sollen
     */
    public void filter(int laenge) {
        for(Iterator<String> it = this.wortliste.iterator(); it.hasNext(); ) {
            String s = it.next();
            if(s.length() < laenge) {
                it.remove();
            }
        }
    }


}
