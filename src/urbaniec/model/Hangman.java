package urbaniec.model;

import com.sun.deploy.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Die Klasse Hangman bildet zusammen mit der Klasse Wortliste die Logik des
 * Hangman-Spiels
 * @author Kacper Urbaniec
 * @version 2017-09-06
 */
public class Hangman {
    private Wortliste liste;
    private String wort;
    private char[] richtig;
    private char[] falsch;
    private char[] zustand;
    private int zug;

    /**
     * Erstellt ein neues Hangman-Objekt.
     * @throws IOException
     */
    public Hangman(String file) throws IOException {
        liste = new Wortliste(file);
        // liste.setPath(file);
        wort = liste.getzWort().toUpperCase();
        richtig = new char[1];
        falsch = new char[1];
        zustand = new char[wort.length()];
        for(int i = 0; i < zustand.length; i++) {
            zustand[i] = '_';
        }
        zug = 0;
    }

    /**
     * Erstellt ein neues Hangman-Objekt.
     * @throws IOException
     */
    public Hangman(String file, ArrayList<String> wortliste) throws IOException {
        liste = new Wortliste(file, wortliste);
        // liste.setPath(file);
        wort = liste.getzWort().toUpperCase();
        richtig = new char[1];
        falsch = new char[1];
        zustand = new char[wort.length()];
        for(int i = 0; i < zustand.length; i++) {
            zustand[i] = '_';
        }
        zug = 0;
    }

    /**
     * Gibt den aktuellen Zustand des gesuchten Wortes zurück.
     * @return - Gibt das zustand Attribut zurück
     */
    public char[] getZustand() {
        return zustand;
    }

    /**
     * Gibt die falschen Eingaben zurück.
     * @return - Gibt das falsch Attribut zurück
     */
    public char[] getFalsch() {
        return falsch;
    }

    /**
     * Gibt das Lösungswort zurück.
     * @return - Das Lösungwort zurück.
     */
    public String getWort() {
        return wort;
    }

    /**
     * Gibt den derzeitigen Spielzug zurück.
     * @return - Der derzeitige Spielzug
     */
    public int getZug() {
        return zug;
    }

    /**
     * Speichert ein neues Wort in die externe Datei.
     * @param wort - Das neue Wort für die externe Datei
     * @throws IOException
     */
    public void setTxt(String wort) throws java.io.IOException {
        liste.setTxt(wort);
    }

    /**
     * Setzt den neuen Namen der Wortliste-Datei.
     * @param path - Der neue Dateiname
     */
    public void setPath(String path) {
        // liste.setPath(path);
        try {
            liste = new Wortliste(path);
        }
        catch (java.io.IOException ioe) {
            System.out.println("Kritischer Fehler");
        }
    }

    public void setWortliste(ArrayList<String> wortliste) {
        liste.setWortliste(wortliste);
    }

    /**
     * Gibt die derzeitige Wortliste zurück.
     * @return - Die derzeitige Wortliste
     */
    public ArrayList<String> getWortliste() {
        return liste.getWortliste();
    }

    /**
     * Überprüft die Eingabe für das Hangman-Spiel. Außerdem gibt die Methode die
     * richtigen/falschen Eingaben in ihre vorgesehenen Arrays.
     * @param eingabe - Die Eingabe des Benutzers
     */
    public void test(char eingabe) {
        eingabe = Character.toUpperCase(eingabe);
        int index = wort.indexOf(eingabe);
        if(index != -1) {
            // Falls Eingabe richtig ist, in das richtig Array-speichern
            if(new String(this.richtig).indexOf(eingabe) == -1) {
                if (this.richtig[0] != '\u0000') {
                    char[] dummy = new char[this.richtig.length + 1];
                    for (int i = 0; i < this.richtig.length; i++) {
                        dummy[i] = this.richtig[i];
                    }
                    this.richtig = new char[dummy.length];
                    for (int i = 0; i < this.richtig.length - 1; i++) {
                        this.richtig[i] = dummy[i];
                    }
                }
                this.richtig[this.richtig.length - 1] = eingabe;
            }
            for(int i = 0; i < wort.length(); i++) {
                if(eingabe == wort.charAt(i)) {
                    zustand[i] = eingabe;
                }
            }
        }
        else {
            zug++;
            // Falls Eingabe falsch ist, in das falsch Array-speichern
            if(new String(this.falsch).indexOf(eingabe) == -1) {
                if(this.falsch[0] != '\u0000') {
                   char[] dummy = new char[this.falsch.length + 1];
                   for (int i = 0; i < this.falsch.length; i++) {
                       dummy[i] = this.falsch[i];
                   }
                    this.falsch = new char[dummy.length];
                    for (int i = 0; i < this.falsch.length - 1; i++) {
                        this.falsch[i] = dummy[i];
                    }
                }
                this.falsch[this.falsch.length-1] = eingabe;
            }
        }
    }
}
