package urbaniec.view;

import javax.swing.*;

/**
 * Erstellt eine Frame für die GUI.
 * @author Kacper Urbaniec
 * @version 2017-09-07
 */
public class HangmanFrame extends JFrame {
    /**
     * Initianiliesert das JFrame.
     * @param titel - Titel des Fensters
     * @param panel - Das zu einbettete Panel
     */
    public HangmanFrame(String titel, JPanel panel) {
        this.setTitle(titel);
        this.setBounds(100, 100, 600, 600);
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
