package urbaniec.view;

import javafx.scene.control.Slider;
import urbaniec.controller.HangmanController;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Die Klasse erstellt ein Panel für das Galgenmänncheng-Spiel.
 *
 * @author Kacper Urbaniec
 * @version 2017-09-07
 */
public class HangmanPanel extends JPanel {
    private HangmanController controller;
    private JCheckBox[] button;
    private JButton[] button2;
    private HangmanGraphics grafik;
    private JLabel[] fehler;
    private JTextField[] zustand;
    private JPanel panelOben;

    /**
     * Setzt den Zustand des Galgenmännchen auf den Wert des Parameters.
     *
     * @param counter - Neuer Zustand des Galgenmännchens
     */
    public void setCounter(int counter) {
        grafik.setCounter(counter);
    }

    /**
     * Gibt den Zustand des counter-Attributes zurück.
     *
     * @return - Der aktuelle Zustand des counter-Attributes
     */
    public int getCounter() {
        return grafik.getCounter();
    }

    /**
     * Setzt den Zustand der Checkbox auf den übergegebenen Parameter.
     *
     * @param state - Der übergebene Paramter
     */
    public void setCheckbox(int i, boolean state) {
        button[i].setEnabled(state);
    }

    /**
     * Diese Methode setzt die Titelleiste des Hangmanspiels.
     *
     * @param zustand - Der derzeitige Zustand des suchenden Wortes
     */
    public void setTitle(char[] zustand) {
        this.zustand = new JTextField[zustand.length];
        this.panelOben.removeAll();
        for (int i = 0; i < zustand.length; i++) {
            this.zustand[i] = new JTextField();
            this.zustand[i].setText("" + zustand[i]);
            this.zustand[i].setFont(this.zustand[i].getFont().deriveFont(20f));
            this.zustand[i].setEditable(false);
            this.panelOben.add(this.zustand[i]);
        }
        this.repaint();
        this.revalidate();
    }

    /**
     * Diese Methode deaktievert die gesuchte Checkbox.
     * @param letter - Der Buchstabe der mit der Checkbox assoziiert wird
     */
    public void disableButton(char letter) {
        char l = 'A';
        int search = 0;
        while (l != letter) {
            l++;
            search++;
        }
        button[search].setEnabled(false);
    }

    /**
     * Diese Methode deaktiviert alle Checkboxen.
     */
    public void disableAllButtons() {
        for(int i = 0; i < button.length; i++) {
            button[i].setEnabled(false);
        }
    }

    /**
     * Diese Methode setzt die Buchstaben in die JLabels für falsche Buchstaben.
     *
     * @param falsch - Falsche Buchstaben des Hangman-Spieles
     */
    public void setFehler(char[] falsch) {
        for (int i = 1; i < falsch.length + 1; i++) {
            fehler[i].setText("" + falsch[i - 1]);
        }
    }

    /**
     * Erzeugt das im Klassenkommentar umschriebene Panel-Objekt.
     */
    public HangmanPanel(HangmanController controller) {
        // LINE_START
        this.controller = controller;
        GridLayout gl = new GridLayout(9, 3);
        JPanel panelLinks = new JPanel(gl);
        panelLinks.setLayout(gl);
        char letter = 'A';
        button = new JCheckBox[26];
        for (int i = 0; i < 26; i++) {
            button[i] = new JCheckBox("" + letter);
            button[i].setActionCommand("" + letter);
            button[i].addActionListener(controller);
            panelLinks.add("" + letter, button[i]);
            letter++;
        }
        // LINE_END
        BorderLayout bl = new BorderLayout();
        JPanel panelRechts = new JPanel(bl);
        fehler = new JLabel[11];
        fehler[0] = new JLabel("Falsche Buchstaben:");
        panelRechts.setLayout(bl);
        panelRechts.add(fehler[0], BorderLayout.PAGE_START);
        GridLayout gl2 = new GridLayout(5, 2);
        JPanel panelRechts2 = new JPanel();
        panelRechts2.setLayout(gl2);
        for (int i = 1; i < 11; i++) {
            fehler[i] = new JLabel();
            panelRechts2.add(fehler[i]);
        }
        panelRechts.add(panelRechts2);
        // PAGE_START
        // JPanel panelOben = new JPanel();
        // FlowLayout po = new FlowLayout();
        // panelOben.setLayout(po);
        // this.add(panelOben, BorderLayout.PAGE_START);
        // PAGE_END
        button2 = new JButton[5];
        button2[0] = new JButton("Neues Spiel");
        button2[1] = new JButton("Wort hinzufügen");
        button2[2] = new JButton("Lösung");
        button2[3] = new JButton("Laden");
        button2[4] = new JButton("Speichern");
        JPanel panelUnten = new JPanel();
        FlowLayout fl2 = new FlowLayout();
        panelUnten.setLayout(fl2);
        for (int i = 0; i < 5; i++) {
            panelUnten.add(button2[i]);
            button2[i].setActionCommand("Unten" + i);
            button2[i].addActionListener(controller);
        }
        // Zustandsleiste
        panelOben = new JPanel();
        FlowLayout po = new FlowLayout();
        panelOben.setLayout(po);
        // Alles vervollständigen
        this.setLayout(new BorderLayout());
        this.add(panelLinks, BorderLayout.LINE_START);
        this.add(panelRechts, BorderLayout.LINE_END);
        grafik = new HangmanGraphics();
        this.add(grafik, BorderLayout.CENTER);
        this.add(panelUnten, BorderLayout.PAGE_END);
        this.add(panelOben, BorderLayout.PAGE_START);



    }
}
