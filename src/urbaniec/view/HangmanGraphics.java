package urbaniec.view;

import com.sun.deploy.util.StringUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Dieses Panel zeichnet das Galgenmännchen für das Hangman-Spiel.
 * @author Kacper Urbaniec
 * @version 2017-09-07
 */
public class HangmanGraphics extends JPanel {
    private int counter;

    /**
     * Erzeugt ein neues Galgenmännchen Objekt
     */
    public HangmanGraphics() {
        this.counter = 0;
    }

    /**
     * Setzt den Zustand des Galgenmännchen auf den Wert des Parameters.
     * @param counter - Neuer Zustand des Galgenmännchens
     */
    public void setCounter(int counter) {
        if(counter > 10) {
            counter = 11;
        }
        this.counter = counter;
    }

    /**
     * Gibt den Zustand des counter-Attributes zurück.
     * @return - Der aktuelle Zustand des counter-Attributes
     */
    public int getCounter() {
        return counter;
    }

    /**
     * Setzt den Zustand des Galgenmännchen auf den Wert des Parameters.
     * @param text - Neuer Zustand des Galgenmännchens, kann ungültig sein
     */
    public void setCounter(String text) {
        int counter = 0;
        if(!text.equals("")) {
            try {
                counter = Integer.parseInt(text);
                if(counter > 10) {
                    counter = 11;
                }
            }
            catch (NumberFormatException nfe) {
                counter = 11;
            }
        }
        this.counter = counter;
    }

    /**
     * Diese Methode wird aufgerufen wenn GUI-Komponente angezeigt werden.
     * @param g - Das zu schützende Graphics-Objekt
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = this.getWidth();
        int height = this.getHeight();
        g.setColor(Color.BLACK);
        switch (this.counter) {
            case 11 :
                g.setColor(Color.RED);
                g.drawString("Fehlerhafter Wert!", width/2-50, height/2 - 10);
                g.drawString("(Zahlenbereich 0-10)", width/2-50, height/2 + 10);
                break;
            case 10 :
                g.drawLine(3*width/5, height/10*4+height/5, 3*width/5+width/8, height/10*4+height/5+height/5);
            case 9 :
                g.drawLine(3*width/5, height/10*4+height/5, 3*width/5-width/8, height/10*4+height/5+height/5);
            case 8 :
                g.drawLine(3*width/5, height/13*4+height/5, 3*width/5+width/10, height/10*4+height/5-height/5);
            case 7 :
                g.drawLine(3*width/5, height/13*4+height/5, 3*width/5-width/10, height/10*4+height/5-height/5);
            case 6 :
                g.drawLine(3*width/5, height/10*2+height/5, 3*width/5, height/10*4+height/5);
            case 5 :
                g.drawOval(3*width/5-width/10, height/10*2, width/5, height/5);
            case 4 :
                g.drawLine(3*width/5, height/10, 3*width/5, height/10*2);
            case 3 :
                g.drawLine(width/5, height/10, 3*width/5, height/10);
                g.drawLine(width/5, height/10*2, width/5 + width/10, height/10);
            case 2 :
                g.drawLine(width/5, 9*height/10, width/5, height/10);
                g.drawLine(width/10, 9*height/10, width/5, 8*height/10);
                g.drawLine(width/5 + width/10, 9*height/10, width/5, 8*height/10);
            case 1 :
                g.setColor(Color.GREEN);
                g.drawLine(0, 9*height/10, width, 9*height/10);
                break;
        }
    }
}
